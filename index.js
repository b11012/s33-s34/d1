/*Load the expressjs module into our application and saved it in a variable called express*/

const express = require("express");
// Create an application with expressjs.
// This creates an application that uses express and stores it as app
// app is our server
const app = express();

const port = 4000;

// middleware
// express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our req body.
// app.use is used to run a method or another function for our expressjs api
app.use(express.json());

// mock data
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "dontTalkAboutMe"
	},
	{
		username: "Luisa",
		email: "stronggirl@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	},
];

// Express has a methods to use as routes corresponding to HTTP methods.
// app.get(<endpoint>, <function for req and res>)

app.get('/', (req, res) => {

	// Once the route is accessed, we can send a response with the use of res.send()
	// res.send() actually combines writeHead() and end().
		// used to send a response to the client and ends the request
	res.send('Hello from my first expressJS API')
	//res.status(200).send('Hello from my first expressJS API')
});

/*M I N I - A C T I V I T Y*/

app.get('/greeting', (req,res) => {

	res.send('Hello from Batch182-Castro')
});

/*End of Mini-Activity*/

// retrieval of the mock database
app.get('/users', (req, res) => {

	//res.send already stringifies for you
	res.send(users);
	// res.json(users);
});


app.post('/users', (req, res) => {

	console.log(req.body); //result : {}

	// simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email : req.body.email,
		password: req.body.password
	}

	// push newUser into users array
	users.push(newUser);
	console.log(users);

	// send the updated users array in the client
	res.send(users);
});

	app.delete('/users', (req, res) => {

		users.pop();
		console.log(users);

		// send the updated users array
		res.send(users);
	});

// PUT Method
// update user's password
// :index - wildcard
// localhost:4000/users/0
app.put('/users/:index', (req,res) => {

	console.log(req.body)

	// an object that contains the value of URL params
	console.log(req.params)

	// parseInt the value of the number coming from req.params
	// ['0'] turns into [0]
	let index = parseInt(req.params.index);

	// users[0].password
	users[index].password = req.body.password;

	res.send(users[index]);
});

/*M I N I - A C T I V I T Y*/ 

app.put('/users/update/:index', (req,res) => {

	let index = parseInt(req.params.index);

	users[index].username = req.body.username;

	res.send(users[index]);
})
// END OF MINI ACTIVITY

// Retrieval of single user

app.get('/users/getSingleUser/:index', (req, res) => {

	console.log(req.params) //result: {index: '1'}
	// let req.params = {index: '1'}

	let index = parseInt(req.params.index)
	// parseInt('1')
	console.log(index); // result: 1

	res.send(users[index]);
	console.log(users[index]);
});




/* A C T I V I T Y  S O L U T I O N */

// Create a new route to get and send items  array in the client (GET ALL ITEMS)
app.get('/items', (req,res) => {

	res.send(items)
});

// Create a new route to create and add a new item object in the items array (CREATE ITEM)
app.post('/items', (req, res) => {

	console.log(req.body); 

	let newItem = {
		name : req.body.name,
		price : req.body.price,
		isActive: req.body.isActive
	}


	items.push(newItem);
	console.log(items);


	res.send(items);
});

//Create a new route which can update the price of a single item in the array (UPDATE ITEM)

app.put('/items/:index', (req,res) => {

	console.log(req.body)

	console.log(req.params)

	let index = parseInt(req.params.index);


	items[index].price = req.body.price;

	res.send(items[index]);
});




/*A C T I V I T Y  # 2*/

/*>> Create a new route with '/items/getSingleItem/:index' endpoint. 
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
    
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client*/

    app.get('/items/getSingleItem/:index', (req, res) => {

    	console.log(req.params) 

    	let index = parseInt(req.params.index)
    
    	res.send(items[index]);
   
    });

/*    >> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
        >> This route should allow us to UPDATE the isActive property of our product.
            -Pass the index number of the item you want to de-activate via the url as url params.
            -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
            -send the updated item in the client*/

   	app.put('/items/archive/:index', (req,res) => {

   		console.log(req.params);

   		let index = parseInt(req.params.index);


   		items[index].isActive = false;

   		res.send(items[index]);
   	});


/*   	>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
   	    >> This route should allow us to UPDATE the isActive property of our product.
   	        -Pass the index number of the item you want to activate via the url as url params.
   	        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
   	        -send the updated item in the client*/

    app.put('/items/activate/:index', (req,res) => {

    	console.log(req.params);

    	let index = parseInt(req.params.index);

    	items[index].isActive = true;

    	// status: <string> "pending" / "completed"
    	
    	res.send(items[index]);
    });


// port
app.listen(port, () => console.log(`Server is running at port ${port}`))